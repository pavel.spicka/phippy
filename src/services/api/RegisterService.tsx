import {API_URL_IAM, RequestRegister, ResponseStatus} from "./Types";

export interface IRegisterService {
    register(registerForm: RequestRegister): Promise<ResponseStatus>;

    isResponseRegister(item: ResponseStatus): item is ResponseStatus
}

export default class RegisterService implements IRegisterService {

    isResponseRegister(item: ResponseStatus): item is ResponseStatus {
        return (item as ResponseStatus).message !== undefined;
    }

    async register(registerForm: RequestRegister): Promise<ResponseStatus> {
        let response = await fetch(API_URL_IAM + "/api/v1/registration", {
            method: 'POST', headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(registerForm)
        });
        return await response.json();
    };
}
