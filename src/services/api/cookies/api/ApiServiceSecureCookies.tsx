import {API_URL, ResponseAllMonuments, ResponseMonument, ResponseStatus} from "../../Types";

export interface IApiServiceSecureCookies {
    randomMonuments(location: string): Promise<ResponseMonument | ResponseStatus>;

    allMonuments(): Promise<ResponseAllMonuments | ResponseStatus>;

    isResponseMonument(item: ResponseMonument | ResponseStatus): item is ResponseMonument

    isResponseAllMonuments(item: ResponseAllMonuments | ResponseStatus): item is ResponseAllMonuments
}

export default class ApiServiceSecureCookies implements IApiServiceSecureCookies {

    isResponseMonument(item: ResponseMonument | ResponseStatus): item is ResponseMonument {
        return (item as ResponseMonument).id !== undefined;
    }

    async randomMonuments(): Promise<ResponseMonument | ResponseStatus> {
        let response = await fetch(API_URL + "/secure/monuments/random-monuments", {
            method: 'GET', headers: {
                'accept': '*/*',
                'Content-Type': 'application/json',
            }
        });
        return await response.json();
    }

    isResponseAllMonuments(item: ResponseAllMonuments | ResponseStatus): item is ResponseAllMonuments {
        return (item as ResponseAllMonuments)[0].id !== undefined;
    }

    async allMonuments(): Promise<ResponseAllMonuments | ResponseStatus> {
        let response = await fetch(API_URL + "/secure/monuments/all-monuments", {
            method: 'GET', headers: {
                'accept': '*/*',
                'Content-Type': 'application/json',
            }
        });
        return await response.json();
    }
}
