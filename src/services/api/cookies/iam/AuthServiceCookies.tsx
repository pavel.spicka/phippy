import {CookieAuth} from "./CookieAuth";
import {API_URL_IAM, RequestLogin, ResponseLogin, ResponseStatus} from "../../Types";

export interface IAuthServiceCookies {
    loginCookies(loginForm: RequestLogin): Promise<ResponseStatus>;

    logout(): Promise<Response>;

    isLogin(): boolean;

    isResponseLogin(item: ResponseLogin | ResponseStatus): item is ResponseLogin
}

export default class AuthServiceCookies implements IAuthServiceCookies {

    isResponseLogin(item: ResponseLogin | ResponseStatus): item is ResponseLogin {
        return (item as ResponseLogin).accessToken !== undefined;
    }

    async loginCookies(loginForm: RequestLogin): Promise<ResponseStatus> {
        let response = await fetch(API_URL_IAM + "/api/v1/auth/cookies/login", {
            method: 'POST', headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }, body: JSON.stringify(loginForm)
        });
        let data = await response.json();
        if (response.status === 200) {
            CookieAuth.setCookie("auth", "true");
            return data;
        }
        return data;
    }

    async logout() {
        return await fetch(API_URL_IAM + "/api/v1/auth/cookies/logout", {
            method: 'POST', headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }
        });
    }

    isLogin() {
        return CookieAuth.getCookie("auth").length !== 0;
    }
}
