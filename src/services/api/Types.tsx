export const API_URL = "https://api.phippy.net";
export const API_URL_IAM = "https://iam.phippy.net";

export enum Status {SUCCESS = "SUCCESS", FAILURE = "FAILURE"}
export enum TokenType {ACCESS = "ACCESS", REFRESH = "REFRESH"}

export type Token = { "tokenType": TokenType, "tokenValue": string, "duration": number, "expiryDate": Date }

export type RequestLogin = { email: string, password: string }
export type ResponseLogin = { "accessToken": Token, "refreshToken": Token }

export type ResponseStatus = { "status": Status, "message": string }

export type RequestRegister = { email: string, password: string }

export type ResponseMonument = {
    "id": number,
    "name": string,
    "country": string,
    "continent": string,
    "coordinates": string,
    "url": string,
    "description": string,
    "linkToPhoto": string
};
export type ResponseAllMonuments = [ResponseMonument];