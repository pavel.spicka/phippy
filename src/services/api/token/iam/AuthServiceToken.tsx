import {CookieAuth} from "../../cookies/iam/CookieAuth";
import {API_URL_IAM, RequestLogin, ResponseLogin, ResponseStatus} from "../../Types";

export interface IAuthServiceToken {
    loginToken(loginForm: RequestLogin): Promise<ResponseLogin | ResponseStatus>;

    logout(): void;

    isLogin(): boolean;

    isResponseLogin(item: ResponseLogin | ResponseStatus): item is ResponseLogin
}

export default class AuthServiceToken implements IAuthServiceToken {

    isResponseLogin(item: ResponseLogin | ResponseStatus): item is ResponseLogin {
        return (item as ResponseLogin).accessToken !== undefined;
    }

    async loginToken(loginForm: RequestLogin): Promise<ResponseLogin | ResponseStatus> {
        let response = await fetch(API_URL_IAM + "/api/v1/auth/token/login", {
            method: 'POST', headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }, body: JSON.stringify(loginForm)
        });
        let data = await response.json();
        if (response.status === 200) {
            CookieAuth.setCookie("accessToken", data.accessToken.tokenValue);
            CookieAuth.setCookie("refreshToken", data.refreshToken.tokenValue);
            return data;
        }
        return data;
    }

    logout() {
        CookieAuth.deleteCookie("accessToken")
        CookieAuth.deleteCookie("refreshToken")
        return true;
    }

    isLogin() {
        return CookieAuth.getCookie("accessToken").length !== 0 && CookieAuth.getCookie("refreshToken").length !== 0;
    }
}
