const FooterComponent = () => {
    return (
        <>
            <footer className="page-footer font-small blue pt-4">
                <div className="footer-copyright text-center py-3">© 2023 Copyright: phippy.net</div>
            </footer>
        </>
    );
};

export default FooterComponent;
