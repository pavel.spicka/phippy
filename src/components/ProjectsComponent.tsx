import {Button, Card, Col, Row} from "react-bootstrap";

const ProjectsComponent = () => {
    return (
        <>
            <br/>
            <br/>
            <Row xs={1} md={2} className="g-4">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title> Phippy</Card.Title>
                            <Card.Text>
                                Front-end application in react.
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <Button href="https://gitlab.com/pavel.spicka/phippy"
                                    variant="primary">Gitlab</Button>
                        </Card.Footer>
                    </Card>
                </Col>
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title> Phippy Thymeleaf</Card.Title>
                            <Card.Text>
                                Front-end application in Java Thymeleaf.
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <Button href="https://gitlab.com/pavel.spicka/phippy_thymeleaf"
                                    variant="primary">Gitlab</Button>
                        </Card.Footer>
                    </Card>
                </Col>
            </Row>
            <br/>
            <br/>
            <Row xs={1} md={2} className="g-4">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title> Phippy Backend</Card.Title>
                            <Card.Text>
                                Back-end application in Java Spring.
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <Button href="https://gitlab.com/pavel.spicka/phippy_backend" variant="primary" style={{marginRight: 10}}>Gitlab</Button>
                            <Button href="https://api.phippy.net/swagger-ui/index.html" variant="success">Swagger</Button>
                        </Card.Footer>
                    </Card>
                </Col>
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title> Phippy iam</Card.Title>
                            <Card.Text>
                                Phippy - Identity and Access Management.
                            </Card.Text>

                        </Card.Body>
                        <Card.Footer>
                            <Button href="https://gitlab.com/pavel.spicka/phippy_iam" variant="primary" style={{marginRight: 10}}>Gitlab</Button>
                            <Button href="https://iam.phippy.net/swagger-ui/index.html" variant="success">Swagger</Button>
                        </Card.Footer>
                    </Card>
                </Col>
            </Row>
            <br/>
            <br/>
            <Row  xs={1} md={2} className="g-4">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title> Phippy Kubernetes</Card.Title>
                            <Card.Text>
                                Yamls for Kubernetes.
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <Button href="https://gitlab.com/pavel.spicka/phippy_kubernetes" variant="primary">Gitlab</Button>
                        </Card.Footer>
                    </Card>
                </Col>
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title> Phippy Ansible</Card.Title>
                            <Card.Text>
                                Ansible repository.
                            </Card.Text>

                        </Card.Body>
                        <Card.Footer>
                            <Button href="https://gitlab.com/pavel.spicka/ansible_phippy"
                                    variant="primary">Gitlab</Button>
                        </Card.Footer>
                    </Card>
                </Col>
            </Row>
        </>
    );
};

export default ProjectsComponent;
