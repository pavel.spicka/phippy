import {Container, Nav, Navbar} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import AuthServiceToken, {IAuthServiceToken} from "../services/api/token/iam/AuthServiceToken";

const NavigationComponent = () => {
    let navigate = useNavigate();
    const authService: IAuthServiceToken = new AuthServiceToken();

    function logout() {
        authService.logout();
        navigate('/')
    }

    return (
        <>
            <Navbar bg="dark" variant="dark">
                <Container fluid>
                    <Navbar.Brand>Phippy</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href='/'>Home</Nav.Link>
                            <Nav.Link href='/projects'>Projects</Nav.Link>
                            {authService.isLogin() &&
                                <Nav.Link href='/administration/all-monuments'>All Monuments</Nav.Link>}
                        </Nav>
                        <Nav>
                            {!authService.isLogin() ?
                                <Nav.Link href='/account/login'>Log In</Nav.Link> :
                                <Nav.Link onClick={() => logout()}>Log out</Nav.Link>}
                        </Nav>
                        <Nav>
                            <Nav.Link href='/account/register'>Register</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>

                </Container>
            </Navbar>
        </>
    );
};

export default NavigationComponent;
