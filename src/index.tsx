import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter, Route, Routes} from "react-router-dom";

import Home from './page/Home';
import Login from './page/account/Login';
import Register from './page/account/Register';
import AllMonuments from './page/administration/AllMonuments';
import Projects from "./page/Projects";

import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Home/>}/>
                <Route path='/account/login' element={<Login/>}/>
                <Route path='/account/register' element={<Register/>}/>
                <Route path='/administration/all-monuments' element={<AllMonuments/>}/>
                <Route path='/projects' element={<Projects/>}/>
            </Routes>
        </BrowserRouter>
    </React.StrictMode>
);
