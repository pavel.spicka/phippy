import {Col, Container, Image, Row} from "react-bootstrap";
import FooterComponent from "../components/FooterComponent";
import HeaderComponent from "../components/HeaderComponent";
import NavigationComponent from "../components/NavigationComponent";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Home() {
    return (
        <>
            <HeaderComponent/>
            <NavigationComponent/>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <br/>
            <Container>
                <br/>
                <br/>
                <Row>
                    <Col xs={6} md={4}>
                        <Image src="profilePicture.jpg" alt="" fluid rounded/>
                    </Col>
                    <Col md={8}>
                        <h5>Pavel Špička</h5>
                        <p>I am a backend developer and I want to use this page to preset my experience.</p>
                    </Col>
                </Row>
                <br/>
                <br/>
                <Row xs={1} md={4}>
                    <Col>
                        <Image src="java.jpg" alt="" fluid rounded/>
                    </Col>
                    <Col>
                        <Image src="spring.jpg" alt="" fluid rounded/>
                    </Col>
                    <Col>
                        <Image src="thymeleaf.jpg" alt="" fluid rounded/>
                    </Col>
                    <Col>
                        <Image src="bootstrap.jpg" alt="" fluid rounded/>
                    </Col>
                </Row>
            </Container>
            <FooterComponent/>
        </>
    );
}

export default Home;