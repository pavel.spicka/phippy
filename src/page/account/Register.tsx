import React, {useState} from 'react'
import {useNavigate} from 'react-router-dom';
import HeaderComponent from "../../components/HeaderComponent";
import NavigationComponent from "../../components/NavigationComponent";
import FooterComponent from "../../components/FooterComponent";
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import RegisterService, {IRegisterService} from "../../services/api/RegisterService";
import {Status} from "../../services/api/Types";

export default function Register() {
    let navigate = useNavigate();
    const iRegisterService : IRegisterService = new RegisterService();

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();

        if (password !== passwordConfirm) {
            toast.error('Passwords do not match!', {
                position: "bottom-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return;
        }

        iRegisterService.register({email, password}).then((response) => {
            if (response.status === Status.SUCCESS) {
                navigate('/');
            } else {
                toast.error(response.message, {
                    position: "bottom-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        });
    }

    return (
        <>
            <HeaderComponent/>
            <NavigationComponent/>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <br/>
            <Container fluid>
                <Row className="justify-content-md-center">
                    <Col xs lg="2">
                        <Form onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Form.Label>Email Address</Form.Label>
                                <Form.Control
                                    type="email"
                                    id="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    id="password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    id="passwordConfirm"
                                    value={passwordConfirm}
                                    onChange={(e) => setPasswordConfirm(e.target.value)}
                                />
                            </Form.Group>
                            <Button variant="primary" type="submit" value="Register">
                                Register
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
            <FooterComponent/>
        </>
    );
}
