import React, {useState} from 'react'
import HeaderComponent from "../../components/HeaderComponent";
import NavigationComponent from "../../components/NavigationComponent";
import FooterComponent from "../../components/FooterComponent";
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {useNavigate} from "react-router-dom";
import AuthServiceToken, {IAuthServiceToken} from "../../services/api/token/iam/AuthServiceToken";

export default function Login() {
    let navigate = useNavigate();
    const authService: IAuthServiceToken = new AuthServiceToken();

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();

        authService.loginToken({email, password}).then((response) => {
            if (authService.isResponseLogin(response)) {
                navigate('/');
            } else {
                toast.error(response.message, {
                    position: "bottom-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        });
    }

    return (
        <>
            <HeaderComponent/>
            <NavigationComponent/>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <br/>
            <Container fluid>
                <Row className="justify-content-md-center">
                    <Col xs lg="2">
                        <Form onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="email"
                                    id="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    placeholder="Enter email"
                                />
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    id="password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    placeholder="Password"
                                />
                            </Form.Group>
                            <Button variant="primary" type="submit" value="Login">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
            <FooterComponent/>
        </>
    );
}
