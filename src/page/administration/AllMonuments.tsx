import FooterComponent from "../../components/FooterComponent";
import HeaderComponent from "../../components/HeaderComponent";
import NavigationComponent from "../../components/NavigationComponent";
import React, {useEffect, useState} from "react";
import Table from 'react-bootstrap/Table';
import {ToastContainer} from "react-toastify";
import {ResponseAllMonuments, ResponseMonument} from "../../services/api/Types";
import ApiServiceSecureToken, {IApiServiceSecureToken} from "../../services/api/token/api/ApiServiceSecureToken";

export default function AllMonuments() {
    const [allMonuments, setAllMonuments] = useState([{} as ResponseMonument] as ResponseAllMonuments);
    const apiService: IApiServiceSecureToken = new ApiServiceSecureToken();

    useEffect(() => {
        apiService.allMonuments().then((response) => {
            if (apiService.isResponseAllMonuments(response)) setAllMonuments(response)
        });
        // eslint-disable-next-line
    }, [])

    return (
        <>
            <HeaderComponent/>
            <NavigationComponent/>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover/>
            <Table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Country</th>
                    <th>Continent</th>
                    <th>Coordinates</th>
                    <th>Link to Wikipedia</th>
                    <th>Description</th>
                    <th>Photos</th>
                </tr>
                </thead>
                <tbody>
                {allMonuments.length !== 1 &&
                    allMonuments.map((monument) => {
                        return (
                            <tr>
                                <td>{monument.name}</td>
                                <td>{monument.country}</td>
                                <td>{monument.continent}</td>
                                <td>{monument.coordinates}</td>
                                <td><a href={monument.url}>Link</a></td>
                                <td>{monument.description}</td>
                                <td><a href={monument.linkToPhoto}>Link</a></td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
            <FooterComponent/>
        </>
    );
}
