import FooterComponent from "../components/FooterComponent";
import HeaderComponent from "../components/HeaderComponent";
import NavigationComponent from "../components/NavigationComponent";
import ProjectsComponent from "../components/ProjectsComponent";
import React from "react";
import {Container} from "react-bootstrap";

export default function Projects() {
    return (
        <>
            <HeaderComponent/>
            <NavigationComponent/>
            <br/>
            <Container>
                <br/>
                <br/>
                <ProjectsComponent/>
            </Container>
            <FooterComponent/>
        </>
    );
}
